from django.urls import path

from webapp.views import FileListView, FileDeleteView, FileUpdateView, FileDetailView, FileCreateView

urlpatterns = [
    path('', FileListView.as_view(), name='index'),
    path('create/', FileCreateView.as_view(), name='create'),
    path('detail/<int:pk>', FileDetailView.as_view(), name='detail'),
    path('update/<int:pk>', FileUpdateView.as_view(), name='update'),
    path('delete/<int:pk>', FileDeleteView.as_view(), name='delete')
    ]

app_name = 'webapp'